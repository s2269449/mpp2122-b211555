#!/bin/bash

# This script performs a regression test comparing the outputs of the serial version of the code 
# to the parallel version. In case they differ, the difference will be printed to the screen.

# First build and run the serial version.

# Colors
RED='\033[1;31m'
GREEN='\033[1;32m'
NC='\033[0m' # no color

cd ./reference-code/ && make && mpirun -n 4 ./percolate 7777 > ser.out && cd ..
echo "Done running reference code!"

make && mpirun -n 4 ./percolate 7777 > par.out && cd ..
echo "Done runnning 2D code!"

file1="reference-code/ser.out"
file2="par.out"
if cmp -s "$file1" "$file2"; then
    echo -e "${GREEN} The text output matches! ${NC}"
else
    echo -e "${RED}The text outputs are different! ${NC}"
    #echo "Output of the diff command:"
    #echo ""
    #diff $file1 $file2
fi

map1="reference-code/map.pgm"
map2="map.pgm"
if cmp -s "$map1" "$map2"; then
    printf "${GREEN}The maps are equal! ${NC}\n"
else
    printf "${RED}The map from "%s" is different from the map from "%s" ${NC} \n" "$file1" "$file2"
    #echo "Output of the diff command:"
    #echo ""
    #diff $map1 $map2
fi
