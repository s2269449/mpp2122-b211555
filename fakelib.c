#include <stdio.h>
#include "percolate.h"

void mp_start(int *rank) {}

void mp_helpmsg(int *rank)
{
    printf("Usage: percolate <seed>\n");
}

void mp_scatter(int map[L][L], int smallmap[M][N], int rank) {}

void mp_gather(int map[L][L], int smallmap[M][N], int rank) {}

void mp_haloswap(int old[M + 2][N + 2], int rank) {}

int mp_updatecell(int x, int y) { return y; }

void mp_addchanges(int *nchangelocal) {}

void mp_addaverages(double *localsum) {}

void mp_stop() {}