# MPP2122-B211555

This project holds the source code for my MPP coursework submission.
The demanded reference output for a run on more than 36 processes can be found in 
`percolate-2466994.out` and `map-2466994.pgm`.
The run with SLURM job id **2466994** was performed with grid size L = 1536 on 64 processors.

### Using the Program on Cirrus
1. **Load all necessary modules**
```
$ module load intel-compilers-19      # intel compiler suite
$ module load mpt                     # message passing toolkit
$ module load ImageMagick             # for visualization (optional)
```
2. **Choose between serial and parallel version**

The Makefile will create an executable when the `make` command is issued.
To compile the parallel version of the code, set 
`SRC=$(PARSRC)` 
in line 30 of the `Makefile`. For a serial version, set 
`SRC=$(SERSRC)`. 
The only difference between the two sources is including either `mplib.c` or `fakelib.c`. 

3. **Specify problem size and proccess topology**

To set the problem size and desired process topology, the first three macro definitions in `percolate.h` can be modified by the user.
```
// Edit these lines to determine problem size and topology
#define L 768    // setting the system size
#define PROC_X 4 // processors in the horizontal direction
#define PROC_Y 4 // processors in the vertical direction
```
The example above would run the computation on a 768 x 768 grid with 16 processors divided into 4 rows and 4 columns.

4. **Command Line Parameters**

To specify command line parameter, simply write them in arbitraty order after the seed number. There are ***three*** command line parameters implemented. 
**autostop** -> automatically stops the calculation in case of zero changes in one iteration
**averages** -> prints out the average value of the map at intervals
**times** -> prints out timing data for the program

5. **Testing for Correctness**

To test for correctness of a program, a simple regression test can be performed.
There are two outputs of the program that can be compared, `map.pgm` and the stdout.
For the regression test to work, the source and reference code have to use the same problem size.
To perform a regression test with `reference-code` as reference, execute the following steps. 
```
# Run the program and write stdout into a file
$ mpirun -n 16 ./percolate 7777 source.out
# Run the same command for the reference code
$ cd reference-code 
$ mpirun -n 16 ./percolate 7777 reference.out
# Compare the outputs
$ diff map.pgm ../map.pgm
$ diff reference.out ../source.out
```
The `diff` command compares the file contents. If the program was correct, there will be no output from `diff` since the files are equal.

6. **Testing for Performance**

To configure the program to print out timing data, specify the `times` command line parameter. To obtain timing data from the reference code, uncomment the lines where timing measurements are made.
The programs will output total execution time, time spent inside the loop, and time spent in initialization and finalization.

###Description of Source and Header Files
`Makefile` -> file used to build executables (serial and parallel)
In the `Makefile` users can choose whether the serial or parallel version of the code should be compiled.

</br>

`percolate.h` -> header file for the main computation
`percolate.c` -> source file for the main computation
In `percolate.h` the problem size and processor topology can be specified.
In `percolate.c`, the main computation with calls to the libraries is implemented.

</br>

`mplib.h` -> header file for the message passing library, included in `percolate.h`
`mplib.c` -> source file for the message passing library 
`mplib` is a library I have written to hide calls to MPI in `percolate.c`. 
This also makes it possible to compile serial and parallel versions of the code with the same codebase.

</br>

`fakelib.h` -> header file for the mock library, included in `percolate.h`
`fakelib.c` -> source file for the mock library 
`fakelib` is a library with mock functions I have written to enable the usage of the same source code for parallel and serial versions.

</br>

`percio.h` -> write out maps to .pgm files for visualization 
The only thing that changed in `percio.h` is the implementation of a new function `mapwritesmall` visualizing maps of size `M x N`.

</br>

`unirand.h` -> random number generator header file
`unirand.c` -> random number generator source file
`unirand` implements a random number generator.
The given implementation has not been modified.

</br>

`reference-code/` -> directory with provided solution using 1D decomposition
This directory contains the given 1D solution used as a reference for correctness and performance testing.

</br>

### Known Limitations

The problem size needs to be divisible without rest by the amount of processors of the respective axis. Any invalid configuration will be caught by the program.

Periodic boundary conditions are not implemented yet.
