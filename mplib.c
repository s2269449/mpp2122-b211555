#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "unirand.h"
#include "percolate.h"

MPI_Comm comm2d;
int prevX, prevY, nextX, nextY; // neighbours in each direction
int i, j, proccount;
int tag = 0;
const int bufsize = M * N;
MPI_Datatype subset;
MPI_Status status[NPROC];
MPI_Request request[NPROC];

void mp_start(int *rank)
{
    if (PROC_X == 1 && PROC_Y == 1)
    {
        printf("Invalid processor configuration, please run serial version instead of parallel version with one processor!\n");
        exit(0);
    }
    if (L % PROC_X != 0)
    {
        printf("Problem size L = %d is not divisible by processor count %d on horizontal axis!\n", L, PROC_X);
        exit(0);
    }
    if (L % PROC_Y != 0)
    {
        printf("Problem size L = %d is not divisible by processor count %d on vertical axis!\n", L, PROC_Y);
        exit(0);
    }
    // MPI variables

    // new variables required for 2D decomposition
    int disp = 1;
    int dims[NDIMS];
    int period[NDIMS];

    // Cartesian Topology
    dims[0] = PROC_X;
    dims[1] = PROC_Y;
    period[0] = FALSE;
    period[1] = FALSE;

    MPI_Init(NULL, NULL);

    // Create Cartesian communicator
    MPI_Cart_create(MPI_COMM_WORLD, NDIMS, dims, period, FALSE, &comm2d);

    // Compute rank and neighbours in comm2d
    MPI_Comm_rank(comm2d, rank);

    MPI_Cart_shift(comm2d, 0, disp, &prevY, &nextY);
    MPI_Cart_shift(comm2d, 1, disp, &prevX, &nextX);

    // check which process has which neighbours
    // printf("Rank: %d Horizontal Neighbours (prev,next): %d %d \n", *rank, prevX, nextX);
    // printf("Rank: %d Vertical Neighbours (prev,next): %d %d \n", *rank, prevY, nextY);
}

void mp_helpmsg(int *rank)
{
    if (*rank == 0)
    {
        printf("Usage: percolate <seed>\n");
    }
    MPI_Finalize();
}

void mp_scatter(int map[L][L], int smallmap[M][N], int rank)
{

    // Datatype to send subsets of map to the processors
    MPI_Type_vector(M, N, L, MPI_INT, &subset);
    MPI_Type_commit(&subset);
    if (rank == 0)
    {
        // Send out Parts of the Grid to all processors
        proccount = 0;

        for (i = 0; i < PROC_X; i++)
        {
            for (j = 0; j < PROC_Y; j++)
            {
                MPI_Issend(&map[i * M][j * N], 1, subset, proccount, 0, comm2d, &request[proccount]);
                proccount++;
            }
        }
    }

    MPI_Recv(&smallmap[0][0], bufsize, MPI_INT, 0, 0, comm2d, &status[i]);

    if (rank == 0)
    {
        MPI_Waitall(NPROC, request, status);
    }
}

int mp_updatecell(int x, int y)
{
    return x;
}

void mp_haloswap(int old[M + 2][N + 2], int rank)
{
    // status and request arrays for halo swaps
    MPI_Status swapstatus[4 * NPROC];
    MPI_Request swaprequest[4 * NPROC];

    // Datatype to send around vertical pieces of the halo to horizontal neighbours
    MPI_Datatype haloY;
    MPI_Type_vector(M, 1, (N + 2), MPI_INT, &haloY); // Count 2, Length 2 and Stride 4
    MPI_Type_commit(&haloY);

    // mp_haloswap(m, n, old, next, prev);
    // Send X halos to prev and next
    MPI_Issend(&old[1][1], N, MPI_INT, prevY, tag, comm2d, &swaprequest[(rank * 4) + 0]);
    MPI_Issend(&old[M][1], N, MPI_INT, nextY, tag, comm2d, &swaprequest[(rank * 4) + 1]);
    MPI_Issend(&old[1][1], 1, haloY, prevX, tag, comm2d, &swaprequest[(rank * 4) + 2]);
    MPI_Issend(&old[1][N], 1, haloY, nextX, tag, comm2d, &swaprequest[(rank * 4) + 3]);

    // receive the halos
    MPI_Recv(&old[0][1], N, MPI_INT, prevY, tag, comm2d, &swapstatus[(rank * 4) + 0]);
    MPI_Recv(&old[M + 1][1], N, MPI_INT, nextY, tag, comm2d, &swapstatus[(rank * 4) + 1]);
    MPI_Recv(&old[1][0], 1, haloY, prevX, tag, comm2d, &swapstatus[(rank * 4) + 2]);
    MPI_Recv(&old[1][N + 1], 1, haloY, nextX, tag, comm2d, &swapstatus[(rank * 4) + 3]);

    MPI_Waitall(4, &swaprequest[rank * 4], &swapstatus[rank * 4]);
}

void mp_addchanges(int *nchangelocal)
{
    int tmp;
    MPI_Allreduce(nchangelocal, &tmp, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    *nchangelocal = tmp;
}

void mp_addaverages(double *localsum)
{
    double tmp;
    MPI_Allreduce(localsum, &tmp, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    *localsum = tmp;
}

void mp_gather(int map[L][L], int smallmap[M][N], int rank)
{
    // Now gather the maps back together
    MPI_Issend(&smallmap[0][0], bufsize, MPI_INT, 0, tag, comm2d, &request[rank]);

    proccount = 0;
    if (rank == 0)
    {
        // receive all messages from the processors
        for (i = 0; i < PROC_X; i++)
        {
            for (j = 0; j < PROC_Y; j++)
            {
                MPI_Recv(&map[i * M][j * N], 1, subset, proccount, tag, comm2d, &status[proccount]);
                proccount++;
            }
        }
    }

    if (rank == 0)
    {
        MPI_Waitall(NPROC, request, status);
    }
}

void mp_stop(void)
{
    MPI_Finalize();
}
