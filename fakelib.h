void mp_start(int *rank);
void mp_helpmsg(int *rank);
void mp_scatter(int map[L][L], int smallmap[M][N], int rank);
int mp_updatecell(int x, int y);
void mp_haloswap(int old[M + 2][N + 2], int rank);
void mp_addchanges(int *nchangelocal);
void mp_addaverages(double *localsum);
void mp_gather(int map[L][L], int smallmap[M][N], int rank);
void mp_stop(void);
