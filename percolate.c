#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <unistd.h>
#include "percolate.h"
#include <time.h>

/*
 * Simple serial program to test for percolation of a cluster.
 */

int main(int argc, char *argv[])
{
  clock_t begin = clock();

  // Define the main arrays for the simulation
  // Example how maps are drawn
  // N is the "faster" index going in horizontal direction.
  // M is the "slower" index going in vertical direction.
  //        _______
  //     M |   |   |
  //     ^ | 3 | 4 |
  //     | |___|___|
  //     | |   |   |
  //     | | 1 | 2 |
  //     | |___|___|
  //       -------> N

  int map[L][L];
  int old[M + 2][N + 2], new[M + 2][N + 2];

  //  Additional array WITHOUT halos for initialisation and IO. This
  //  is of size LxL because, even in our parallel program, we do
  //  these two steps in serial
  int smallmap[M][N];

  // Check for command line parameters
  int argcount;
  int autostop = 0;
  int averages = 0;
  int times = 0;
  for (argcount = 2; argcount < 5; argcount++)
  {
    if (argv[argcount])
    {
      if (!strcmp(argv[argcount], "autostop"))
      {
        autostop = 1;
      }
      if (!strcmp(argv[argcount], "averages"))
      {
        averages = 1;
      }
      if (!strcmp(argv[argcount], "times"))
      {
        times = 1;
      }
    }
  }
  // Variables that define the simulation

  int seed;
  double rho;

  // Local variables

  int i, j, nhole, step, maxstep, oldval, newval;
  int nchangelocal, nchange, printfreq;
  double localsum; // float necessary because of large values
  int itop, ibot, perc;
  double r;
  int rank = 0;

  // Initialize MPI for parallel code
  mp_start(&rank);

  if (argc < 2)
  {
    mp_helpmsg(&rank);
    return 0;
  }

  /*
   *  Update for a fixed number of steps, periodically report progress
   */

  maxstep = 5 * L;
  printfreq = 100;

  // Initialize map
  if (rank == 0)
  {
    printf("percolate: running on %d process(es)\n", NPROC);

    rho = 0.4040;

    seed = atoi(argv[1]);

    printf("percolate: L = %d, rho = %f, seed = %d, maxstep = %d\n",
           L, rho, seed, maxstep);

    rinit(seed);
    nhole = 0;

    for (i = 0; i < L; i++)
    {
      for (j = 0; j < L; j++)
      {
        r = uni();
        if (r < rho)
        {
          map[i][j] = 0;
        }
        else
        {
          nhole++;
          map[i][j] = nhole;
        }
      }
    }

    printf("percolate: rho = %f, actual density = %f\n",
           rho, 1.0 - ((double)nhole) / ((double)L * L));
  }

  /*
   *  Now scatter map to smallmap
   */

  mp_scatter(map, smallmap, rank);

  /*
   * Initialise the old array: copy the LxL array smallmap to the centre of
   * old, and set the halo values to zero.
   */

  for (i = 1; i <= M; i++)
  {
    for (j = 1; j <= N; j++)
    {
      // use x for parallel and y for serial computation
      int x = smallmap[i - 1][j - 1];
      int y = map[i - 1][j - 1];
      old[i][j] = mp_updatecell(x, y);
    }
  }

  for (i = 0; i <= M + 1; i++) // zero the bottom and top halos
  {
    old[i][0] = 0;
    old[i][N + 1] = 0;
  }

  for (j = 0; j <= N + 1; j++) // zero the left and right halos
  {
    old[0][j] = 0;
    old[M + 1][j] = 0;
  }

  step = 1;
  nchange = 1;

  clock_t begin_loop = clock();
  while (step <= maxstep)
  {
    nchange = 0;
    nchangelocal = 0;
    localsum = 0;

    // swap halos for parallel program
    mp_haloswap(old, rank);

    for (i = 1; i <= M; i++)
    {
      for (j = 1; j <= N; j++)
      {
        oldval = old[i][j];
        newval = oldval;

        /*
         * Set new[i][j] to be the maximum value of old[i][j]
         * and its four nearest neighbours
         */

        if (oldval != 0)
        {
          if (old[i][j - 1] > newval)
            newval = old[i][j - 1];
          if (old[i][j + 1] > newval)
            newval = old[i][j + 1];
          if (old[i - 1][j] > newval)
            newval = old[i - 1][j];
          if (old[i + 1][j] > newval)
            newval = old[i + 1][j];

          if (newval != oldval)
          {
            ++nchangelocal;
          }
        }
        new[i][j] = newval;
        localsum += newval;
      }
    }

    // Calculate changes and average values
    mp_addchanges(&nchangelocal);
    if (averages)
    {
      mp_addaverages(&localsum);
    }

    if (autostop && (nchangelocal == 0))
    {
      for (i = 1; i <= M; i++)
      {
        for (j = 1; j <= N; j++)
        {
          // no clue whether this will work correctly
          smallmap[i - 1][j - 1] = old[i][j];
          map[i - 1][j - 1] = old[i][j];
        }
      }

      /*
       *  Now gather smallmap back to map
       */
      mp_gather(map, smallmap, rank);
      //  Stop the calculation and terminate the program
      if (rank == 0)
      {
        printf("##############################################################\n");
        printf("%d changes on step %d detected, terminating calculation! \n",
               nchangelocal, step);

        if (averages)
        {
          printf("Average value of the map on final step %d is %f\n",
                 step, (double)(localsum / L * L));
        }
        if (times)
        {
          if (rank == 0)
          {
            clock_t end_loop = clock();
            clock_t end = clock();
            double loop_time = (double)(end_loop - begin_loop) / CLOCKS_PER_SEC;
            double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
            double init_time = time_spent - loop_time;
            printf("Loop Time: %f \n", loop_time);
            printf("Init Time: %f \n", init_time);
            printf("Total Time: %f \n", time_spent);
          }
        }
        printf("##############################################################\n");
        perc = 0;

        for (itop = 0; itop < L; itop++)
        {
          if (map[itop][L - 1] > 0)
          {
            for (ibot = 0; ibot < L; ibot++)
            {
              if (map[ibot][0] == map[itop][L - 1])
              {
                perc = 1;
              }
            }
          }
        }

        if (perc != 0)
        {
          printf("percolate: cluster DOES percolate\n");
        }
        else
        {
          printf("percolate: cluster DOES NOT percolate\n");
        }

        /*
         *  Write the map to the file "map.pgm", displaying the two
         *  largest clusters. If the last argument here was 3, it would
         *  display the three largest clusters etc. The picture looks
         *  cleanest with only a single cluster, but multiple clusters
         *  are useful for debugging.
         */

        mapwrite("map.pgm", map, 4);
      }
      mp_stop();
      exit(0);
    }

    //  Report progress every now and then

    if (step % printfreq == 0)
    {
      if (rank == 0)
      {
        if (averages)
        {
          printf("percolate: average value of the map on step %d is %f\n",
                 step, (double)(localsum / L * L));
        }
        printf("percolate: changes on step %d is %d\n",
               step, nchangelocal);
      }
    }

    //  Copy back in preparation for next step, omitting halos

    for (i = 1; i <= M; i++)
    {
      for (j = 1; j <= N; j++)
      {
        old[i][j] = new[i][j];
      }
    }

    step++;
  }

  clock_t end_loop = clock();
  /*
   *  We set a maximum number of steps to ensure the algorithm always
   *  terminates. However, if we hit this limit before the algorithm
   *  has finished then there must have been a problem (e.g. the value
   *  of maxstep is too small)
   */

  if (rank == 0)
  {
    if (nchange != 0)
    {
      printf("percolate: WARNING max steps = %d reached but nchange != 0\n",
             maxstep);
    }
  }

  /*
   *  Copy the centre of old, excluding the halos, into smallmap
   */

  for (i = 1; i <= M; i++)
  {
    for (j = 1; j <= N; j++)
    {
      smallmap[i - 1][j - 1] = old[i][j];
      map[i - 1][j - 1] = old[i][j]; // for serial computation
    }
  }

  mp_gather(map, smallmap, rank);

  //  Test to see if percolation occurred by looking for positive numbers
  //  that appear on both the top and bottom edges

  if (rank == 0)
  {
    perc = 0;

    for (itop = 0; itop < L; itop++)
    {
      if (map[itop][L - 1] > 0)
      {
        for (ibot = 0; ibot < L; ibot++)
        {
          if (map[ibot][0] == map[itop][L - 1])
          {
            perc = 1;
          }
        }
      }
    }

    if (perc != 0)
    {
      printf("percolate: cluster DOES percolate\n");
    }
    else
    {
      printf("percolate: cluster DOES NOT percolate\n");
    }

    /*
     *  Write the map to the file "map.pgm", displaying the two
     *  largest clusters. If the last argument here was 3, it would
     *  display the three largest clusters etc. The picture looks
     *  cleanest with only a single cluster, but multiple clusters
     *  are useful for debugging.
     */

    mapwrite("map.pgm", map, 4);
  }

  mp_stop();
  if (times)
  {
    if (rank == 0)
    {
      clock_t end = clock();
      double loop_time = (double)(end_loop - begin_loop) / CLOCKS_PER_SEC;
      double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
      double init_time = time_spent - loop_time;
      printf("Loop Time: %f \n", loop_time);
      printf("Init Time: %f \n", init_time);
      printf("Total Time: %f \n", time_spent);
    }
  }
  return 0;
}
