MF=	Makefile


CC=	mpicc
CFLAGS=	-O3 -Wall

LFLAGS= $(CFLAGS)

EXE=	percolate

INC= \
	percolate.h \
	mplib.h \
	fakelib.h \
	unirand.h

# Source files for serial version
SERSRC= \
	percolate.c \
	percio.c \
	unirand.c \
	fakelib.c

# Source files for parallel version
PARSRC= \
	percolate.c \
	percio.c \
	unirand.c \
	mplib.c


SRC=$(PARSRC)

#
# No need to edit below this line
#

.SUFFIXES:
.SUFFIXES: .c .o

OBJ=	$(SRC:.c=.o)

.c.o:
	$(CC) $(CFLAGS) -c $<

all:	$(EXE)

$(OBJ):	$(INC)

$(EXE):	$(OBJ)
	$(CC) $(LFLAGS) -o $@ $(OBJ)

$(OBJ):	$(MF)

clean:
	rm -f $(EXE) $(OBJ) core
