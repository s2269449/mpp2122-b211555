/*
 *  Main header file for percolation code.
 */

// Edit these lines to determine problem size and topology
#define L 768    // setting the system size
#define PROC_X 4 // processors in the horizontal direction
#define PROC_Y 4 // processors in the vertical direction

// No need to edit anything here
#define NPROC (PROC_X * PROC_Y)
#define M (L / PROC_X)
#define N (L / PROC_Y)
#define NDIMS 2
#define TRUE 1
#define FALSE 0

/*
 * Include message passing and fake libraries
 */
#include "mplib.h"
#include "fakelib.h"

/*
 *  Visualisation
 */

void mapwrite(char *percfile, int map[L][L], int ncluster);
void mapwritesmall(char *percfile, int map[M][N], int ncluster);
void mapwritedynamic(char *percfile, int **map, int l, int ncluster);

/*
 *  Random numbers
 */

void rinit(int ijkl);
float uni(void);
